<?php

define('RESTRICTED', 1);

//untuk memastikan session route tidak kosong, dan memulai session baru (ini diluar session login)
if (!session_id())
{
    session_start();
}

//Disini untuk memanggil function atau class dari folder apps/
require 'apps/config/app.php';
require_once 'apps/model/class.auth.php';

//here our routes
$page   = (!empty($_GET['page'])) ? $_GET['page'] : null;
$action = (!empty($_GET['action'])) ? $_GET['action'] : null;

switch ($page) {
	case 'auth':
		if ($action == 'login') {
			# Route login disini contoh require 'nama_folder/namafile.php'
		}
		elseif ($action == 'register') {
			# route register
		}
		else{
			#route ini untuk pengalihan apabila tidak mengarahkan ke login/register (buatlah form error 404 dan arahkan ke form tersebut)
		}
		break;

	case 'index':
		# arahkan ke route dashboard dan berilah session login/ apabila tidak ada session maka akan diarahkan ke route login
		break;
	
	default:
		# pada default ini arahkan ke dashboardnya ketika sudah login
		break;
}